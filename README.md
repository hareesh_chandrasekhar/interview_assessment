**NextLabs** Assessment
\n I have uploaded the python codes for 2 questions(part 1 and part 2) in the gitlab repo(https://gitlab.com/nextlabs1/interview_assessment). For Part 2 which requires deploying on streamlink, I have done that and I have shared the live link below
\n #Question 1:
\n Write a regex to extract all the numbers with orange color background from the below text in italics.
{"orders":[{"id":1},{"id":2},{"id":3},{"id":4},{"id":5},{"id":6},{"id":7},{"id":8},{"id":9},{"id":10},{"id":11},{"id":648},{"id":649},{"id":650},{"id":651},{"id":652},{"id":653}],"errors":[{"code":3,"message":"[PHP Warning #2] count(): Parameter must be an array or an object that implements Countable (153)"}]}

Answer: The code does the same. I used regex along with checking the condition  :
The code for part 1 is uploaded in the files section

\n **#Question 2**
1. Train a machine learning model (preferably with a neural network) that 
predicts the customer who is going to be checked in. Once done, please test 
the prediction with below test data
Answer: The data consisted of 82580 observations across 30 features
The goal of this is to build machine learning classification model which predicts whether the customer will checkin to the hotel or not.
I have used 3 different models like Logistic Regression,Random Forest and also Artificial Neural Network and obtained a good accuracy value
The EDA performed and the model building is present in the following link
https://gitlab.com/nextlabs1/interview_assessment/-/blob/main/NextLabs-part2.ipynb


\n **More Bonus points (You can write answers to these in ReadMe)**
\n **Question 1**
Write about any difficult problem that you solved. (According to us difficult - is something which 90% of people would have only 10% probability in getting a similarly good solution). 
Answer: 
Handling of Imbalanced Dataset:
Once the dataset was highly imbalanced 90% of the data was from Class 1 and only 10% belonged to class 0
This difficulty was overcome by applying different techniques like 
a)Oversampling
b)Undersampling
c)SMOTE
In Model Building,trying different models which gives different accuracy values and also hypertuning the parameters 

\n **Question 2**
Explain back propagation and tell us how you handle a dataset if 4 out of 30 parameters have null values more than 40 percentage

Answer:part 1:Back-propagation is the essence of neural net training. It is the practice of fine-tuning the weights of a neural net based on the error rate (i.e. loss) obtained in the previous epoch (i.e. iteration). Proper tuning of the weights ensures lower error rates, making the model reliable by increasing its generalization.
 we need an activation function that determines the activation value at every node in the neural net
 We also need a hypothesis function that determines what the input to the activation function is.

 Back-propagation is all about feeding the  loss backwards in such a way that we can fine-tune the weights based on which the optimization function (Gradient Descent in our example) will help us find the weights that will — hopefully — yield a smaller loss in the next iteration.

 feeding backward will happen through the partial derivatives of these functions
 A neural network is a group of connected I/O units where each connection has a weight associated with its computer programs. It helps you to build predictive models from large databases. This model builds upon the human nervous system. It helps you to conduct image understanding, human learning, computer speech, etc.

 Backpropagation in neural network is a short form for “backward propagation of errors.” It is a standard method of training artificial neural networks. This method helps calculate the gradient of a loss function with respect to all the weights in the network.

 The Back propagation algorithm in neural network computes the gradient of the loss function for a single weight by the chain rule. It efficiently computes one layer at a time, unlike a native direct computation. It computes the gradient, but it does not define how the gradient is used. It generalizes the computation in the delta rule.

Part2: If 4 features out of 30 features have null values more than 40 percentage,those null values cannot be dropped.
We need to see how those features are distributed 
If the data is normally distributed those null values can be replaced by the mean value of that particular feature
If the data is skewed ,those null values can be replaced with median value
There are other methods like KNN Imputation,Forward fill or Backward fill


\n **Steps for Deployment**
\n 1)After checking which model gives the best accuracy value,we will finalise that model
\n 2)**Pickle** needs to be imported
\n 3)Save the model into .sav file
\n 4)Load the model using pickle library
\n 5)Then in order to create a web application for user interface ,Streamlit library was used
\n 6)In that file we import the loaded model and create user inputs to be provided and after clicking the submit button, \n \n the result is displayed on the screen


\n **Live link of the deployed model**
